function [ODR,P,Xs,Ys]=blob_detection(R,b,O,s1,s2)
b=ceil(.5*b);
GreenC=O(:,:,2);
Siz2=size(GreenC);
eval=3;
ODR=R(:,:,5);
%G=(imresize(G,s1));
Siz=size(R(:,:,5));
c=ceil(b/(2*s1));
RedP=[zeros(c,Siz2(2)+2*c);zeros(Siz2(1),c) GreenC zeros(Siz2(1),c);zeros(c,Siz2(2)+2*c)];
%figure(100)
%imshow(G1)
xloc=zeros(1,eval);
yloc=zeros(1,eval);
Mask=rgb2gray(imresize(O,s1));
Mask=(imbinarize(Mask,0.01));
er=double(imerode(Mask,strel('diamond',5)));
Pre=er.*R(:,:,5);
%borders are set to zero
Pre(1,:)=0;
Pre(2,:)=0;
Pre(3,:)=0;
Pre(4,:)=0;
Pre(5,:)=0;
Pre(6,:)=0;

Pre(Siz(1),:)=0;
Pre(Siz(1)-1,:)=0;
Pre(Siz(1)-2,:)=0;
Pre(Siz(1)-3,:)=0;
Pre(Siz(1)-4,:)=0;
Pre(Siz(1)-5,:)=0;
Pre(Siz(1)-6,:)=0;
Pre(Siz(1)-7,:)=0;
%%

%Laplacian of gaussian is used to detect bolbs
sigma=4;
h = fspecial('log', sigma*6, sigma);
B = -imfilter(Pre,h);
% figure
% imagesc(B); 
T=graythresh(B);
B(B < T) = 0;
figure(1)
imagesc(B);
pause();
close all;
Bd = imdilate(B,strel('disk',2)); 
 %figure(98)
 %imagesc(Bd);
P = (B == Bd) .* B;

%figure(97)
%imagesc(P)

% location of the blob is obtained and checked for higher intensity values
% as it is used for optic disc detection
MaxScores=zeros(1,eval);
for i=1:eval
[M1,loc]=max(P);
[~,X]=max(M1);
Y=loc(X);

xloc(i)=ceil(X/s2);
yloc(i)=ceil(Y/s2);
%figure(50+i)
%imagesc(P)
P(Y,X)=0;

tempMat=RedP(yloc(i):yloc(i)+2*c,xloc(i):xloc(i)+2*c);
%figure(i)
%imshow(tempMat/max(max(tempMat)))

MaxScores(i)=sum(sum(tempMat));
end
[~,Winner]=max(MaxScores);
Ys=ceil(yloc(Winner)*(1/1));
Xs=ceil(xloc(Winner)*(1/1));

%%
%draw on top of orginal image to display location and localised region
b=2*b;
 imshow(O)
 hold on;
 rectangle('Position',[ceil(Xs-ceil(b*(1/s2)/2)) ceil(Ys-ceil(b*(1/s1)/2)) ceil(b*(1/s2)) ceil(b*(1/s1))],...
           'Curvature',[0,0],...
          'LineWidth',2,'LineStyle','-','EdgeColor','b')
 plot(Xs,Ys,'b*')
 %plot(xo,yo,'g*')
 hold off
