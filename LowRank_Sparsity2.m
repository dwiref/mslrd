%% Multi-scale Low Rank Matrix Decomposition on Hanning Matrices
%

function[X_it]=LowRank_Sparsity2(Y)


setPath

%% Set Parameters

%N=size(Y,1);
Y_size = size(Y);
% L = log2(N);  % Number of levels
dorandshift = 1; 
skip=2;
%FOV = [N,N]; % Matrix Size

nIter = 1000; % Number of iterations

%rho = 10; % ADMM parameter

rho =0.5; % ADMM parameter

%% Generate Multiscale block Sizes
L = ceil(max( log2( Y_size(1:2) ) ));

% Generate block sizes
block_sizes = [ min( 2.^(0:skip:L)', Y_size(1)) , min(2.^(0:skip:L)',Y_size(2))];
%block_sizes = [2.^(0:2:max_L)', 2.^(0:2:max_L)'];
% block_sizes= [[[ceil(.2*min(Y_size)) Y_size(1)]',[ceil(.2*min(Y_size))]];
%Y_size(2)]']; %v1
% block_sizes=[[ ceil(.25*min(Y_size)) Y_size(1)]',[ ceil(.2*min(Y_size)) Y_size(2)]'];
%block_sizes=[[1 2 4 8 16 32 64 128 320]',[1 2 4 8 16 32 64 128 240]'];
%b=ceil(.25*min(Y_size));
%b=block_sizes(1);%for localization v 1
%block_sizes = [block_sizes; N^2, 1];
%disp('Block sizes:');
%disp(block_sizes)
levels = size(block_sizes,1);

ms = block_sizes(:,1);

ns = block_sizes(:,2);

%lambdas = sqrt(ms)+ sqrt(ns)+ sqrt(log(prod(repmat(FOV,[levels,1]),2)./max(ms,ns)));
bs = prod( repmat(Y_size, [levels,1]) ./ block_sizes, 2 );
%ms = [ms; N*N];
% ns = [ns; 1];
% bs = [bs; 1];
%Penalties
lambdas = sqrt(ms) + sqrt(ns) + sqrt( log2( bs .* min( ms, ns ) ) );
%lambdas = sqrt(ms)+ sqrt(ns)+ sqrt(log2(prod(repmat(FOV,[levels,1]),2).*min(ms,ns)));

%% Initialize Operator

FOVl = [Y_size,levels];
level_dim = length(Y_size) + 1;

% Get summation operator
A = @(x) sum( x, level_dim ); % Summation Operator
AT = @(x) repmat( x, [ones(1,level_dim-1), levels] ); % Adjoint Operator

%% Iterations

X_it = zeros(FOVl);
Z_it = zeros(FOVl);
U_it = zeros(FOVl);


k = 0;
K = 1;
rho_k = rho;

for it = 1:nIter
    
    % Data consistency
    X_it = (1 / levels) * AT( Y - A( Z_it - U_it ) ) + Z_it - U_it;
    
    % Level-wise block threshold
    parfor l = 1:levels
     XU = X_it(:,:,l) + U_it(:,:,l);
     r = []; 
     if (dorandshift)
            [XU, r] = randshift( XU );
     end
    XU = blockSVT( XU, block_sizes(l,:), lambdas(l) / rho_k);     

     if (dorandshift)
            XU = randunshift( XU, r );
     end   
     
         Z_it(:,:,l)=XU;
    end
    
    % Update dual
    U_it = U_it - Z_it + X_it;
   
    
    k = k + 1;
    if (k == K)
        rho_k = rho_k * 2;
        U_it = U_it / 2;
        K = K * 2;
        k = 0;
    end
    
       figure(24),
       % imshow3(mat2gray(abs(X_it)),[],[1,levels]),
       imshow3(mat2gray(abs(X_it)),[]),
       %imshow3(mat2gray(abs(X_it)),[]),
       %imshow(Y-X_it(:,:,levels))
end
