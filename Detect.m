function [loc1,loc2]=Detect(R,b,O,s1,s2)

Mask=rgb2gray(imresize(O,s1));
Mask=(imbinarize(Mask,0.01));
er=double(imerode(Mask,strel('disk',7)));
Pre=er.*R(:,:,2);
% figure
% imshow(er)
%%
sigma=4;
h = fspecial('log', sigma*6, sigma);
B = -imfilter(Pre,h);
% figure
% imagesc(B); 
T=graythresh(B);
B(B < T) = 0;
% figure
% imagesc(B);
Bd = imdilate(B,strel('disk',b)); 
% figure
% imagesc(Bd);
P = (B == Bd) .* B;
% figure
% imagesc(P)
[M1,loc]=max(P);
[~,loc2]=max(M1);
loc1=loc(loc2);
loc1=ceil(loc1*(1/s2));
loc2=ceil(loc2*(1/s1));
b=ceil(b);
%%
 
 imshow(O)
 hold on;
 rectangle('Position',[ceil(loc2-ceil(b*(1/s2)/2)) ceil(loc1-ceil(b*(1/s1)/2)) ceil(b*(1/s2)) ceil(b*(1/s1))],...
           'Curvature',[0,0],...
          'LineWidth',2,'LineStyle','-','EdgeColor','r')
 plot(loc2,loc1,'r*')
 hold off
