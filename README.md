# Multi-scale Low Rank Image Decomposition for Fundus Image Processing

This project contains MATLAB code that I and a friend worked on in my final year of college. 
(Aditya - If you're on GitLab, let me know so I can add you as an author)
Reference: https://arxiv.org/abs/1507.08751 
https://ieeexplore.ieee.org/document/7439735
F. Ong, M. Lustig, "Beyond Low Rank + Sparse: Multi-scale Low Rank Matrix Decomposition," IEEE Journal of Selected Topics in Signal Processing, Vol.10, 627-687, IEEE, March 2016

The code is largely derived from the work done by the author of the above work, Frank Ong. Due credit to him for that. Any modifications (of which there are few), are to better tailor the code
to deal with large fundus images. 
## Motivation

Multi-scale Low Rank decomposition works well in isolating image features, especially when they are of different sizes. Fundus images have 3 major features that can be extracted using this:
```
1. Optic Disc
2. Exudates, lesions, hemorrhages etc.
3. Blood vessels
```

## Methodology

Decomposing a fundus image over 11 scales successfully seperates retinal features. 
Optic Disc detection using blob detection is implemented. 

## Further Work

As such, it should be possible to perform exudate segmentation for Diabetic Retinopathy. Will be explored in the foreseeable future.