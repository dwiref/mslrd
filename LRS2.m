clc
clear 
close all
load handel
      
setPath
%% Result 1

inputIm='image';
outputIm='ISIC_0000';
for i=1:474
tic
Infilename=strcat([inputIm,num2str(i,'%03d')],'.png');
%InBfilename=strcat([inputIm,num2str(i,'%03d')],'_valid.png');
Outfilename=[outputIm,num2str(i,'%03d')];
scaleFactor=0.5;
scaleFactor2=0.5;
Original=im2double((imread(Infilename)));
%Bmask=imresize(im2double((imread(InBfilename))),scaleFactor2);
I=imresize(Original,scaleFactor1);
I=I(:,:,3);
[R,b]=LowRank_Sparsity2(I);
fig=figure(i);
[loc1,loc2]=Detect(R,b,Original,scaleFactor1,scaleFactor2);
print(fig,Outfilename,'-djpeg')
close(i)
toc
end